import 'package:flutter/material.dart';

import 'package:f_wallet/src/splash_screen.dart';
import 'package:f_wallet/src/utils/args_parser.dart';

void main(List<String>? args) {
  PassArgsParser.initialize(args);
  runApp(const SplashScreen());
}
