import 'package:flutter/widgets.dart';

typedef DevicePixelRatioBuilderCallback = Widget Function(
  BuildContext context,
  double pixelRatio,
);

class DevicePixelRatioBuilder extends StatefulWidget {
  final DevicePixelRatioBuilderCallback builder;

  const DevicePixelRatioBuilder({super.key, required this.builder});

  @override
  State<DevicePixelRatioBuilder> createState() =>
      _DevicePixelRatioBuilderState();
}

class _DevicePixelRatioBuilderState extends State<DevicePixelRatioBuilder>
    with WidgetsBindingObserver {
  double? _currentOrientation;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    _currentOrientation = MediaQuery.of(context).devicePixelRatio;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final pixelRatio = MediaQuery.of(context).devicePixelRatio;
      if (pixelRatio != _currentOrientation) {
        setState(() => _currentOrientation = pixelRatio);
      }
    });
  }

  @override
  void didUpdateWidget(covariant DevicePixelRatioBuilder oldWidget) {
    if (oldWidget.builder != widget.builder) setState(() {});
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final pixelRatio =
        _currentOrientation = MediaQuery.of(context).devicePixelRatio;
    return widget.builder.call(context, pixelRatio);
  }
}
