import 'package:flutter/material.dart';

class CardShape extends StatelessWidget {
  final Widget child;
  final Color color;

  static final _borderRadius = BorderRadius.circular(16);

  const CardShape({
    super.key,
    required this.child,
    this.color = Colors.transparent,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 8,
      borderRadius: _borderRadius,
      child: ClipRRect(
        borderRadius: _borderRadius,
        child: Material(
          color: color,
          child: child,
        ),
      ),
    );
  }
}
