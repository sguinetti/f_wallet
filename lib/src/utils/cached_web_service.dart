import 'dart:typed_data';

import 'package:pkpass/pkpass_web_service.dart';

import 'package:f_wallet/src/utils/hive_boxes.dart';

class CachedWebService extends PkPassWebService {
  CachedWebService(super.metadata);

  @override
  Future<Uint8List?> getLatestVersion([DateTime? modifiedSince]) async {
    final startTimestamp = DateTime.now();
    final lastTimestamp = HiveBoxes.webService.get(metadata.serialNumber);
    final result = super.getLatestVersion(lastTimestamp);
    await HiveBoxes.webService.put(metadata.serialNumber, startTimestamp);
    return result;
  }
}
