import 'dart:ui';

import 'package:pkpass/pkpass.dart';

extension FlutterTextAlign on PassTextAlign {
  TextAlign toTextAlign() {
    switch (this) {
      case PassTextAlign.left:
        return TextAlign.left;
      case PassTextAlign.center:
        return TextAlign.center;
      case PassTextAlign.right:
        return TextAlign.right;
      default:
        return TextAlign.start;
    }
  }
}
