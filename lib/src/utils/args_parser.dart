import 'dart:developer';

import 'package:args/args.dart';
import 'package:file_selector/file_selector.dart';

abstract class PassArgsParser {
  const PassArgsParser._();

  static final ArgParser parser = ArgParser();

  static ArgResults? result;

  static void initialize(List<String>? args) {
    if (args == null) return;
    try {
      result = parser.parse(args);
    } catch (e) {
      log('Error parsing arguments', error: e);
    }
  }

  static bool get hasFiles {
    final import = result?.arguments;
    if (import != null && import.isNotEmpty) return true;
    return false;
  }

  static List<XFile> get files {
    final import = result?.arguments;
    if (import == null) return [];
    return import.map((e) => XFile(e)).toList();
  }
}
