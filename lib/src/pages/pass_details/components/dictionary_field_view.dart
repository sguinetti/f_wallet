import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:pkpass/pkpass.dart';

import 'package:f_wallet/src/utils/locale_locle.dart';
import 'package:f_wallet/src/utils/pass_text_alignment.dart';
import 'package:f_wallet/src/widgets/linkified_pass_text.dart';

class DictionaryFieldView extends StatelessWidget {
  final DictionaryField field;
  final PassFile file;

  const DictionaryFieldView({
    super.key,
    required this.field,
    required this.file,
  });

  @override
  Widget build(BuildContext context) {
    final locale = Localizations.localeOf(context).toLocale();
    final label = field.getLocalizedLabel(file, locale);
    final value = field.value.getLocalizedValue(file, locale);

    String? displayValue;
    if (value is StringDictionaryValue) {
      displayValue = value.string;
    } else if (value is DateTimeDictionaryValue) {
      displayValue =
          DateFormat(null, locale.toLanguageTag()).format(value.dateTime);
    } else if (value is NumberDictionaryValue) {
      displayValue = value.number.toString();
    }

    final align = field.textAlignment?.toTextAlign();

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (label != null)
            Text(
              label,
              style: const TextStyle(fontSize: 16),
              textAlign: align,
            ),
          if (displayValue != null)
            LinkifiedPassText(
              displayValue,
              textAlign: align,
            ),
        ],
      ),
    );
  }
}
